/** \author M. Charbonnier
 *  \file This file defines the Home-Cinema-Interface class.
 */
#ifndef _IR_DISPLAY_DRIVER_H_
#define _IR_DISPLAY_DRIVER_H_

#include <queue>
#include <string>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/serial.h>
#include <termios.h>

#include <boost/smart_ptr.hpp>
#include <boost/system/error_code.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>

#include <iostream>
#include <unistd.h>
#include <logcplusplus.hpp>

#include "irdd_cmd.h"
#include "irdd_err.h"
#include "irdd_commands.h"
class IrDD
{
public:
  typedef unsigned int IrMode2;

  static const int _Baud = 35714;       ///< this is the baud rate of the device.
  static const int _Max_Buffer = 200;
  static const int _maxTimeout = 500000;///< communication timeout.
  static const int _Time_Scale = 8;     ///< number of microseconds for each increment of raw data.

public:
  IrDD();
  virtual ~IrDD();
    
  ///////////////////////
  //      Accessors    //
  ///////////////////////
  /** get the device path
   *  @return device path
   */
  const std::string& get_Device_Path() const { return _devicePath; }
  
  /// returns the current error
  boost::system::error_code get_Error()
  {
    boost::system::error_code outError;
    boost::unique_lock<boost::mutex>(_mutex);
    outError = _error;
    return outError;
  }
  
  ///////////////////////
  //   HW Operations   //
  ///////////////////////
public:
  int open(const std::string&); ///< Opens the device
  int sendCmd(const IrDD_Cmd&); ///< Sends a Command to the device
  /// Returns the Lirc Mode2 compatible data corresponding to one pulse
  unsigned int get_Next_Ir_Data(){return 0;} 

  const boost::system::error_code& getError(){return _error;}

protected:
  virtual void setError(const boost::system::error_code& inError)
  {
    boost::unique_lock<boost::mutex>(_mutex);
    _error = inError;
  }

  virtual void onOpen()=0; ///<called when a device is successfully opened

  void setError(int& inCode, const boost::system::error_category& inCategory)
  {
    boost::system::error_code tmpError;
    tmpError.assign(inCode, inCategory);
    setError(tmpError);
  }
  
  virtual void irCodeHandler() = 0;
  IrMode2 getCurrentIrData(){return _currentIrData;}
private:
  void setBaud(int);  ///< set a baud rate without using the asio function.
  void close(); ///< close the device
  
  void read( std::size_t ); ///< read the socket.
  void readHandler(IrDDBuffer_Ptr inBuffer, const boost::system::error_code& ec, std::size_t nChar);

  void write(IrDDBuffer_Ptr inBuffer); ///< write data on the scoket
  void writeHandler(IrDDBuffer_Ptr inBuffer, const boost::system::error_code& ec, std::size_t nChar);
  
  void clearCmdQueue();
  bool getIsOpen(){ boost::unique_lock<boost::mutex>(_openMutex); bool ans = _isOpen; return ans;}
  
  void setAcknowledge( bool ack )
    { boost::unique_lock<boost::mutex>(_writeMutex); _waitAcknowledge = ack; }
  bool getAcknowledge( )
    { boost::unique_lock<boost::mutex>(_writeMutex); return _waitAcknowledge; }
  /// returns true if a write can be started
  bool requestWrite()
    { boost::unique_lock<boost::mutex>(_writeMutex);
      bool ok = (_waitAcknowledge == false);
      _waitAcknowledge = true;
      return ok; }

protected:
  boost::asio::io_service _io;
  LgCpp::Logger _logger;

private:
  boost::system::error_code _error;
  std::string _devicePath;
  boost::asio::io_service::work _ioWork;
  boost::asio::serial_port _serialPort;
  boost::thread* _thread;
  boost::mutex _writeMutex;
  boost::mutex _dataMutex;
  boost::mutex _openMutex;
  
  std::queue< IrDDBuffer_Ptr > _pendingData;
  bool _isOpen;
  bool _waitAcknowledge;
  bool _readingIrData;
  IrMode2 _currentIrData; ///< true if a pulse is incomming, false if not
};

#endif //_IR_DISPLAY_DRIVER_H_
