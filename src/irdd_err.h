#include <boost/system/error_code.hpp>

#ifndef _IRDD_ERR_H_
#define _IRDD_ERR_H_

enum IrDD_ErrCodes
{
    _NoError = 0,
    _DeviceAlreadyOpened,
    _UnknownCommand,
    _ScreenBusy,
    _Timeout,
    _number_Of_Errors
};

class IrDD_ErrCategory : public boost::system::error_category
{
public:
    const char * name() const {return "Irdd_ErrCategory";}
    std::string message( int ev ) const
    {
        std::string ans;
        switch(ev)
        {
            case(_NoError):
                ans = "no error";
                break;
            case(_DeviceAlreadyOpened):
                ans = "The device is already open";
                break;
            case(_UnknownCommand):
                ans = "Unknown command";
                break;
            case(_ScreenBusy):
                ans = "Screen busy";
                break;
            case(_Timeout):
                ans = "Device timed out";
                break;
            default:
                ans = "unknown error";
                break;
        }
        return ans;
    }
};

#endif
