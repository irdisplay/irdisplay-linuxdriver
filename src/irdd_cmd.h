/** \author M. Charbonnier
 *  \file This file defines a command structure and commands for the BigHCI.
 */

#ifndef _IRDD_CMD_H_
#define _IRDD_CMD_H_

#include <stdint.h>
#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <algorithm>
#include <boost/shared_ptr.hpp>

typedef uint8_t IrDDByte;
typedef std::vector<IrDDByte> IrDDBuffer;
typedef boost::shared_ptr< std::vector<IrDDByte> > IrDDBuffer_Ptr;

class IrDD_Cmd
{
/////////////
// Members //
/////////////
public:

  static const unsigned char _Max_Length = 255; ///< maximal length of a command.
  
  /// device commands
  enum base_Commands
  {
    do_Nothing = 0,
    print_Screen,
    command_Screen,
    start_Screen_Animation,
    stop_Screen_Animation,
    start_Screen_Scroll,
    stop_Screen_Scroll,
    end_Commands
  };
private:
  IrDDByte _cmd;   /// Command identifier
  std::vector<IrDDByte> _parameter; /// Parameter

/////////////
// Methods //
/////////////
public:
  IrDD_Cmd();
  ~IrDD_Cmd();

  ///////////////////////
  //      Accessors    //
  ///////////////////////
  char get_Cmd() const {return _cmd;} ///< return the infrared command.
  void set_Cmd(IrDDByte in_Cmd){_cmd = in_Cmd;} ///< sets the infrared command.
  
  /// get the parameter of the command.
  std::vector<IrDDByte> get_Parameters() const {return _parameter;}
  
  /// get the parameter of length.
  unsigned char get_Parameters_Length() const
    {return ( _parameter.size() > _Max_Length )? _Max_Length: (unsigned char)_parameter.size();}
  
  ///////////////////////
  //  Data Operations  //
  ///////////////////////
public:
  int add_ASCII_To_Parameters(const std::string & in_ASCII);          /// Add an ASCII string to Parameters
  int add_Bytes_To_Parameters(const std::vector<IrDDByte> & in_Data); /// Add Binary Data to Parameters
  int clear_Parameters(); /// Clear the command parameters
  int clear_Cmd();        /// Clear the command identifier
  void getSerializedData( IrDDBuffer_Ptr ) const;
};
  
#endif //_IRDD_CMD_H_
