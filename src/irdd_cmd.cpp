#include "irdd_cmd.h"
#include "irdd_commands.h"

IrDD_Cmd::IrDD_Cmd()
{
  _parameter.reserve( _Max_Length );
  clear_Parameters();
  clear_Cmd();
}

IrDD_Cmd::~IrDD_Cmd()
{
}

///////////////////////
//  Data Operations  //
///////////////////////

/** This function is called to add a character string to the parameters it uses strcmp to work
 *  @param in_ASCII : data to add to the parameters
 *  @return Number of data added to the parameters
 */
int IrDD_Cmd::add_ASCII_To_Parameters(const std::string & in_ASCII)
{
  int i;
  
  for(i = 0; ( i < in_ASCII.size() ) && ( _parameter.size() < _Max_Length ) ; ++i)
  {
    _parameter.push_back( in_ASCII[i] );
  }
  
  return i;
}

/** @param in_Data : Bytes to copy to the parameters
 *  @return -1 on failure, number of Bytes copied otherwise
 */
int IrDD_Cmd::add_Bytes_To_Parameters(const std::vector<IrDDByte> & in_Data)
{
  int i;
  
  for(i = 0; ( i < in_Data.size() ) && ( _parameter.size() < _Max_Length ) ; ++i)
  {
    _parameter.push_back( in_Data[i] );
  }
  
  return i;
}

int IrDD_Cmd::clear_Parameters()
{
  _parameter.clear();
  return 0;
}

int IrDD_Cmd::clear_Cmd()
{
  clear_Parameters();
  _cmd = do_Nothing;
  return 0;
}

void IrDD_Cmd::getSerializedData( IrDDBuffer_Ptr outData ) const
{
  outData->clear();
  outData->reserve( _Max_Length + 2);
  outData->push_back( _cmd );
  // if the command is CMD_CHAR_TO_SCREEN, we must specify the number of bytes sent
  if(_cmd == CMD_CHAR_TO_SCREEN)
    outData->push_back( get_Parameters_Length() );
  
  outData->insert( outData->end(), _parameter.begin(), _parameter.end() );
}


