#include "ir-display-driver.h"

IrDD::IrDD(): _thread(NULL), _io(), _ioWork(_io), _serialPort(_io),
  _waitAcknowledge(false), _readingIrData(false), _isOpen(false), _logger("IrdDevice")
{
  _thread = new boost::thread( boost::bind(&boost::asio::io_service::run, &_io) );
}

IrDD::~IrDD()
{
  close();
  _io.stop();
  if( _thread != NULL )
  {
    _thread->join();
    delete _thread;
    _thread = NULL;
  }
}

int IrDD::open(const std::string& inDevicePath)
{
  int ans = 0;
  if( _openMutex.try_lock() )
  {
    if(inDevicePath != _devicePath && _isOpen)
    {
      try{
        _serialPort.close();
      }
      catch(boost::system::system_error error)
      {
        setError(error.code());
        ans = -1;
      }
      _isOpen = false;
      
      setAcknowledge(false);
    }
    if(!_isOpen)
    {
      try{
        _devicePath = inDevicePath;
        _serialPort.open( inDevicePath );
        try{
          _serialPort.set_option( boost::asio::serial_port_base::baud_rate(_Baud));
        }
        catch(boost::system::system_error error)
        {
          // special baud rates cannot be setted with asio::serial_port_base::baud_rate. Try an other way.
          setBaud(_Baud);
        }
        _isOpen = true;
        read(1);
        onOpen();
      }
      catch(boost::system::error_code error)
      {
        setError(error);
        ans = -1;
      }
      catch(boost::system::system_error error)
      {
        setError(error.code());
        ans = -1;
      }
    }
    _openMutex.unlock();
  }
}

void IrDD::close()
{
  boost::unique_lock<boost::mutex> lk(_openMutex);
  if( _isOpen )
  {
    try
    {_serialPort.close();}
    catch(boost::system::system_error error)
    {}
    _isOpen = false;
    
    setAcknowledge(false);
  }
}

int IrDD::sendCmd(const IrDD_Cmd& inCmd)
{
  IrDDBuffer_Ptr cmdData(new IrDDBuffer);
  inCmd.getSerializedData( cmdData );
  if( !getIsOpen() )
    open(_devicePath);
    
  if(getIsOpen())
  {
    if(requestWrite())
      write(cmdData);
    else
    {
      boost::unique_lock<boost::mutex> lk(_dataMutex);
      _pendingData.push(cmdData);
    }
  }
}

void IrDD::clearCmdQueue()
{
  std::queue< IrDDBuffer_Ptr > empty;
  boost::unique_lock<boost::mutex> lk(_dataMutex);
  std::swap(_pendingData, empty);
}

void IrDD::write(IrDDBuffer_Ptr inBuffer)
{
  boost::asio::async_write( _serialPort, boost::asio::buffer(*inBuffer),
    boost::bind(&IrDD::writeHandler, this, inBuffer, _1, _2) );
}

void IrDD::read( std::size_t inSize )
{
  IrDDBuffer_Ptr readBuffer(new IrDDBuffer);
  readBuffer->resize(inSize);

  boost::asio::async_read( _serialPort, boost::asio::buffer(*readBuffer), 
    boost::bind(&IrDD::readHandler, this, readBuffer, _1, _2) );
}

void IrDD::readHandler(IrDDBuffer_Ptr inBuffer, const boost::system::error_code& ec, std::size_t nChar)
{
  if( !ec )
  {
    std::size_t incommingSize = 1;
    ///@todo TODO add a time up in case of acknowledge or the end of the data never comes
    // timeup should start with _readingIrData set to true or setAcknowledge(True)
    // timeup should be stopped with _readingIrData cleared or setAcknowledge(False)
    //
    // on timeup, _readingIrData should be set to false as well as acknowledge
    // _currentIrData must be cleared (requires mutex)
  
    ///@todo TODO Notice: on screen busy error, two bytes are sent from the device, only one
    // is handled here --> source of error since it will be considered as a command, probably as IR data

    if( _readingIrData )
    {
      _readingIrData = false;
      IrMode2 tmpData = 0;
      for( int i = 0; i < inBuffer->size(); ++i )
      {
        tmpData <<= 8;
        tmpData += (*inBuffer)[i];
      }
      tmpData *= _Time_Scale;
      _currentIrData += tmpData;
      
      std::ostringstream ssMessage;
      ssMessage << "received ir code: " << _currentIrData;
      _logger.logIt(ssMessage, LgCpp::Logger::logDEBUG);
      
      irCodeHandler();
    }
    else
    {
      unsigned char command;
      command = inBuffer->front();
      // IR data received
      if(  command == IR_SPACE_1_BYTE || command == IR_PULSE_1_BYTE ||
           command == IR_SPACE_3_BYTE || command == IR_PULSE_3_BYTE )
      {
        _readingIrData = true;
        if( command == IR_SPACE_3_BYTE || command == IR_PULSE_3_BYTE )
          incommingSize = 3;
        _currentIrData = (command & 0x01) ? 0x01000000 : 0;
      }
      // acknowledge, unknown command error, any other answer
      else if(command != SCREEN_BUSY)
      {
        if(command == UNKNOWN_COMMAND)
          _logger.logIt("from device: unknown command", LgCpp::Logger::logERROR);
        else if (command != ACKNOWLEDGE)
        {
          std::ostringstream ssMessage;
          ssMessage << "unknown answer from device: " << int(command);
          _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
        }
        boost::unique_lock<boost::mutex> lkData(_dataMutex);
        if( !_pendingData.empty() )
        {
          write( _pendingData.front() );
          _pendingData.pop();
        }
        else
          setAcknowledge(false);
      }
      // screen busy error
      else
      {
        _logger.logIt("screen Busy", LgCpp::Logger::logERROR);
        
        IrDDBuffer_Ptr cmdData(new IrDDBuffer);
        cmdData->clear();
        cmdData->reserve(1);
        cmdData->push_back(CMD_CLEAR_ERROR);
        write(cmdData);
      }
    }
    read( incommingSize );
  }
  else
  {
    setError(ec);
    close();
  }
}

void IrDD::writeHandler(IrDDBuffer_Ptr inBuffer, const boost::system::error_code& ec, std::size_t nChar)
{
  if( !ec )
  {
  }
  else
    setError(ec);
}

/** special baud rates cannot be setted with asio::serial_port_base::baud_rate. This is an other way.
 *  @param[in] inBaud: baud rate to set
 *  @throw boost::system::error_code
 */
void IrDD::setBaud( int inBaud )
{
  int ans = 0;
  int spFd = _serialPort.native();
  
  termios tmp_Termios;
  serial_struct tmp_Serial;

  if ( tcgetattr(spFd, &tmp_Termios) < 0)
  {
    setError( errno, boost::system::system_category() );
    throw(_error);
  }
  
  // set flags to 38400Bauds, 8bits words, Ignore Modem Control Signal, Enable Receive
  // This is required to enable custom speeds
  tmp_Termios.c_cflag = B38400 | CS8 | CLOCAL | CREAD;
  // Compile (?) Flags
  cfmakeraw(&tmp_Termios);
  // Set the new configuration with immediate effect (TCSANOW)
  if ( tcsetattr(spFd,TCSANOW,&tmp_Termios) < 0 )
  {
    setError( errno, boost::system::system_category() );
    throw(_error);
  }
  
  if ( ioctl(spFd, TIOCGSERIAL, &tmp_Serial) < 0 )
  {
    setError( errno, boost::system::system_category() );
    throw(_error);
  }
  // set new parameters
  tmp_Serial.flags |= ASYNC_SPD_CUST;
  tmp_Serial.custom_divisor = tmp_Serial.baud_base / _Baud;
  if ( ioctl(spFd, TIOCSSERIAL, &tmp_Serial) )
  {
    setError( errno, boost::system::system_category() );
    throw(_error);
  }
}

