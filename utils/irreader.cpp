#include <signal.h>
#include <logcplusplus.hpp>
#include <string>
#include <iostream>
#include <boost/program_options.hpp>
#include "irreader.h"

class Receiver: public IrDD
{
  void onOpen()
  {
    _logger.logIt("Connected", LgCpp::Logger::logINFO);
  }
  
  void irCodeHandler()
  { 
    IrDD::IrMode2 irData = getCurrentIrData();
    bool pulse = (0xFF000000 & irData) != 0;
    int duration = 0x00FFFFFF & irData;
    
    std::ostringstream ssMessage;
    ssMessage << "State: " << pulse << " | Duration: " << duration << "us";
    _logger.logIt(ssMessage, LgCpp::Logger::logINFO);
  }
};

bool gRun = true;

void niam( int sig )
{
  gRun = false;
}


int main( int argc, char **argv )
{
  signal( SIGTERM, niam );
  signal( SIGINT, niam );
  std::string path = "";
  
  LgCpp::Sink stdOutput( std::cout );
  stdOutput.setLogSeverity( LgCpp::Logger::logDEBUG );
  
  LgCpp::Logger logger("Main");
  
  boost::program_options::options_description options; ///< options descriction
  boost::program_options::variables_map       inputOptions; ///< options values
  
  options.add_options()
    ("help,h", "show this help message")
    ("path,p", boost::program_options::value<std::string>(), "path to the device");
  
  boost::program_options::store(boost::program_options::parse_command_line
            ( argc, argv, options), inputOptions );
  
  if( inputOptions.count("help") )
  {
    std::cout << options << std::endl;
    gRun = false;
  }
  if( inputOptions.count("path") )
  {
    path = inputOptions["path"].as<std::string>();
  }
  
  Receiver irReceiver;
  
  if((path != "") && (gRun == true))
  {
    std::ostringstream ssMessage;
    ssMessage << "Connecting to device: " << path;
    logger.logIt(ssMessage, LgCpp::Logger::logINFO);
    gRun = (irReceiver.open(path) >= 0);
    if (gRun == true)
      logger.logIt("Connected", LgCpp::Logger::logINFO);
    else
      logger.logIt("An error occured", LgCpp::Logger::logINFO);
  }
  else
  {
    gRun = false;
  }
  
  
  while(gRun == true)
  {
    usleep(2000);
  }
  logger.logIt("Leaving", LgCpp::Logger::logINFO);
}
